# Remarques diverses : 


- Vous pouvez tout à fait remplacer des parties de code si elles ne conviennent pas, ou choisir d'utiliser autre chose que bootstrap par exemple.

- Les résultats de recherche proviennent d'un dataset d'exemple de Best Buy. Prenez ces données simplement comme un Lorem Ipsum un peu plus élaboré. Normalement ce sont des produits de startups qui sont affichés.

- L'objectif de l'utilisateur qui visite le site n'est pas d'acheter des produits, mais surtout de comprendre leur proposition de valeur. 

- Ne pas se préoccuper de la page Timesheets

- En cas de problème technique (projet de base ne se lance pas, JSON mal formé...), vous pouvez contacter Nicolas Perez (nicolas.p@iot-valley.fr)





